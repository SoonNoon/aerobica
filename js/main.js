$(document).ready(function () {
    $(function () {
        if ($('span').is('.spincrement')) {
            var show = true;
            var countbox = "#counts";
            $(window).on("scroll load resize", function () {
                if (!show) return false;
                var w_top = $(window).scrollTop();
                var e_top = $(countbox).offset().top;
                var w_height = $(window).height();
                var d_height = $(document).height();
                var e_height = $(countbox).outerHeight();
                if (w_top + 200 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height) {
                    if (!$('.spincrement').hasClass('active')) {
                        $('.spincrement').addClass('active');
                        $('#first').spincrement({duration: 1100});
                        $('#second').delay(700).spincrement({duration: 1000});
                        $('#third').delay(1400).spincrement({duration: 1200});
                        $('#fourth').delay(2100).spincrement({duration: 1000});
                        $('#fifth').delay(2800).spincrement({duration: 1000});
                    }
                    show = false;
                }
            });
        }
    });
    $(function () {
        $('#scrolling').click(function () {
            $('body, html').animate({scrollTop: $(window).height()}, 500);
        });
    });
    $(function () {
        if ($('a').is('.single_image')) {
            $("a.single_image").fancybox({helpers: {media: {}}});
        }
    });
    $(function () {
        if ($('div').is('#makeMeScrollable')) {
            $('#makeMeScrollable').smoothDivScroll({
                mousewheelScrolling: false,
                manualContinuousScrolling: true,
                autoScrollingMode: "onStart",
                hotSpotScrolling: false,
                touchScrolling: true,
            });
        }
    });
    $(function () {
        var dropDownBtn = $('.dropdown-menu-container > .dropdown-btn'),
            dropDownMenu = $('.dropdown-menu-nav, .dropdown-menu-trasnparent'),
            dropDownItem = $('.list-item-trasnparent, .list-item');
        $(document).mouseup(function (e) {
            if (!dropDownMenu.is(e.target) && dropDownMenu.has(e.target).length === 0) {
                dropDownMenu.removeClass('active');
                dropDownBtn.removeClass('active');
            } else {
                dropDownItem.click(function () {
                    dropDownMenu.removeClass('active');
                });
            }
            dropDownBtn.click(function () {
                if ($(this).hasClass('active') && dropDownMenu.hasClass('active')) {
                    $(this).removeClass('active');
                    dropDownMenu.removeClass('active');
                } else {
                    $(this).addClass('active');
                    dropDownMenu.addClass('active');
                }
            });
        });
        $(window).scroll(function () {
            var body = $('body').scrollTop();
            if (body > 5) {
                dropDownMenu.removeClass('active');
                dropDownBtn.removeClass('active');
            }
        });
    });
    $(function () {
        var dropDownBtn = $('.dropdown-min'), dropDownMenu = $('.dropdown-min-menu'), dropDownItem = $('.drop-item');
        dropDownBtn.click(function () {
            if ($(this).hasClass('active') && dropDownMenu.hasClass('active')) {
                $(this).removeClass('active');
                dropDownMenu.removeClass('active');
            } else {
                $(this).addClass('active');
                dropDownMenu.addClass('active');
            }
        });
    });
    $(function () {
        var openBtn = $('.mobile-page-menu-topbar > .fa-bars');
        var topbar = $('.transparent-topbar');
        var blueTopBar = $('#mobile-menu-container');
        var closeBtn = $('.mobile-page-menu > .fa-times');
        var callBtn = $('.mobile-item').find('.callback-button');
        var menu = $('.mobile-page-menu');
        $(document).mouseup(function (e) {
            if (!menu.is(e.target) && menu.has(e.target).length === 0) {
                menu.removeClass('active');
                topbar.css({'display': 'block'});
                blueTopBar.removeClass('active');
            } else {
                callBtn.click(function () {
                    menu.removeClass('active');
                    topbar.css({'display': 'block'});
                    blueTopBar.removeClass('active');
                });
            }
            openBtn.click(function () {
                menu.addClass('active');
                topbar.css({'display': 'none'});
                blueTopBar.addClass('active');
            });
            closeBtn.click(function () {
                menu.removeClass('active');
                topbar.css({'display': 'block'});
                blueTopBar.removeClass('active');
            });
        });
    });
    $(function () {
        var openBtn = $('.tablet-page-menu-topbar > .fa-bars');
        var topbar = $('.transparent-topbar-tablet > .phone-container');
        var blueTopBar = $('#tablet-menu-container > .phone-container');
        var closeBtn = $('.tablet-page-menu > .fa-times');
        var callBtn = $('.tablet-item').find('.callback-button');
        var menu = $('.tablet-page-menu');
        $(document).mouseup(function (e) {
            openBtn.click(function () {
                menu.addClass('active');
                topbar.addClass('active');
                blueTopBar.addClass('active');
            });
            closeBtn.click(function () {
                menu.removeClass('active');
                topbar.removeClass('active');
                blueTopBar.removeClass('active');
            });
            if (!menu.is(e.target) && menu.has(e.target).length === 0) {
                menu.removeClass('active');
                topbar.addClass('active');
                blueTopBar.removeClass('active');
            } else {
                callBtn.click(function () {
                    menu.removeClass('active');
                    topbar.removeClass('active');
                    blueTopBar.removeClass('active');
                });
            }
        });
    });
    $(function () {
        jQuery('.overflow-body').click(function () {
            $('.reserve').removeClass('active');
            $('.call-back').removeClass('active');
            $(this).fadeOut(100);
            $('.more').removeClass('active');
        });
        $('.reserve-btn').click(function () {
            $(".overflow-body").fadeIn(100);
            $('.reserve').addClass('active').find('#direction').val($(this).parent().find('.section-title').text());
        });
        $('.more-btn').click(function () {
            $(".overflow-body").fadeIn(100);
            $('.more').addClass('active').find("#moreTitle").val($(this).parent().find('.sales-content').text());
        });
        $('.reserve').find('.fa-times').bind('click', function () {
            $('.reserve').removeClass('active');
            $('.overflow-body').fadeOut(100);
        });
        $('.more').find('.fa-times').bind('click', function () {
            $('.more').removeClass('active');
            $('.overflow-body').fadeOut(100);
        });
        $('.callback-button').click(function () {
            $(".overflow-body").fadeIn(100);
            $('.call-back').addClass('active');
        });
        $('.call-back').find('.fa-times').bind('click', function () {
            $('.call-back').removeClass('active');
            $('.overflow-body').fadeOut(100);
            $('.transparent-topbar-tablet > .phone-container').removeClass('active');
        });
    });
    $(function () {
        $(document).on('click', '[data-target-popapnews]',function () {
            var tdpopap = $(this).attr('data-target-popapnews');
            $('#' + tdpopap + '').removeClass('active');
            $('#' + tdpopap + '').addClass('vizit');
        });
    });
    $(function () {
        $('[data-target]').click(function () {
            var target = $(this).attr('data-target');
            if (target === 'close') {
                $('.modal-container').fadeOut(300);
                $('.modal-container .modal').fadeOut(300);
            } else {
                $('.modal-container').fadeIn(300);
                $('.modal-container .modal' + target).fadeIn(300);
            }
        });
    });
    $(function () {
        $(window).scroll(function () {
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            $('.team-img-container').each(function (i) {
                var bottom_of_object = $(this).offset().top + $(this).outerHeight() - '400';
                if (bottom_of_window > bottom_of_object) {
                    setInterval(function () {
                        $('.team-img-container img').addClass('active');
                    });
                }
            });
        });
    });
    $(function () {
        var leftBlock = $('#left-block');
        leftBlock.hover(function () {
            $(this).addClass('full-width');
            $('#right-block').addClass('smaller');
            $('#center-block').addClass('smaller');
        }, function () {
            $(this).removeClass('full-width');
            $('#right-block').removeClass('smaller');
            $('#center-block').removeClass('smaller');
        });
        var centerBlock = $('#center-block');
        centerBlock.hover(function () {
            $(this).addClass('full-width');
            $('#right-block').addClass('smaller');
            $('#left-block').addClass('smaller');
        }, function () {
            $(this).removeClass('full-width');
            $('#right-block').removeClass('smaller');
            $('#left-block').removeClass('smaller');
        });
    });
});
$(document).ready(function () {
    $('.dropdown > .btn').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).siblings('#menu').removeClass('active');
        } else {
            $(this).addClass('active');
            $('#menu').addClass('active');
        }
    });
});
$(document).ready(function () {
    $(function () {
        $('.overflow-body').click(function () {
            $('.accordion').removeClass('modal');
            $('.another-group').removeClass('active');
            $(this).fadeOut(100);
        });
        $('.another-group').click(function () {
            $('.overflow-body').fadeIn(100);
            $('.accordion').addClass('modal');
        });
        $('.accordion').find('.fa-times').bind('click', function () {
            $('.accordion').removeClass('modal');
            $('.overflow-body').fadeOut(100);
        });
    });
});
$(document).ready(function () {
    $('.tab-container').tab();
    $(function () {
        $('.tabs-bottom').click(function () {
            if ($('.mobile-tabs').hasClass('active')) {
                $('.mobile-tabs').removeClass('active');
                $('.tabs-bottom > .fa-angle-down').removeClass('active');
            } else {
                $('.mobile-tabs').addClass('active');
                $('.tabs-bottom > .fa-angle-down').addClass('active');
            }
        });
        $('.mobile-tabs > .modal-tabs > ul > li').click(function () {
            $('.mobile-tabs').removeClass('active');
            $('.tabs-bottom > .fa-angle-down').removeClass('active');
        });
        $(window).scroll(function () {
            var body = $('body').scrollTop();
            if (body > 5) {
                $('.mobile-tabs').removeClass('active');
                $('.tabs-bottom > .fa-angle-down').removeClass('active');
            }
        });
    });
});
$(document).ready(function () {
    $('.fa-info').mouseleave(function () {
        if ($(this).hasClass('active')) {
            $(this).parent().children('.info-block').removeClass('active');
            $(this).removeClass('active');
        }
    });
    $('.fa-info').mouseenter(function () {
        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $(this).parent().children('.info-block').addClass('active');
        }
    });


    $(".dropdown-btn").hover(
        function () {
            $(".dropdown-menu-trasnparent").addClass("active");
            $(".dropdown-menu-nav").addClass("active");
        }
    );

    $(window).hover(
        function (event) {
            if ($(event.target).closest(".dropdown-menu-trasnparent").length == 1 || $(event.target).closest(".dropdown-menu-nav").length == 1 || $(event.target).closest(".dropdown-menu-container").length == 1) {
                $(".dropdown-menu-trasnparent").addClass("active");
                $(".dropdown-menu-nav").addClass("active");
            }

        },
        function (event) {
            if ($(event.target).closest(".dropdown-menu-trasnparent").length == 0 || $(event.target).closest(".navigation").length == 0 || $(event.target).closest(".dropdown-menu-nav").length == 1 || $(event.target).closest(".dropdown-menu-container").length == 0) {
                $(".dropdown-menu-trasnparent").removeClass("active");
                $(".dropdown-menu-nav").removeClass("active");
            }
            event.stopPropagation();
        }
    );

    $(document).click(function (event) {
            if ($(event.target).closest(".dropdown-menu-trasnparent").length == 0) {
                $(".dropdown-menu-trasnparent").removeClass("active");
            }
        }
    );


    $('[data-url="tel:+375 29 672-20-02"]').on('click', function () {

        yaCounter41594534.reachGoal('num1');

    });


    $('[data-url="viber://chat?number=+375296722002"]').on('click', function () {

        yaCounter41594534.reachGoal('viber');

    });


    $('[data-url="http://t.me/aerobikaby"]').on('click', function () {

        yaCounter41594534.reachGoal('tel');

    });


    $('.callback-button').on('click', function () {

        yaCounter41594534.reachGoal('perez');

    });


    $('.send').on('click', function () {

        yaCounter41594534.reachGoal('otprav');

    });


    $('.reserve-btn').on('click', function () {

        yaCounter41594534.reachGoal('bron');
    });


    $('[data-url="tel:+375 29 673-05-33"]').on('click', function () {

        yaCounter41594534.reachGoal('num2');

    });


    $('[data-url="tel:+375 29 104-02-02"]').on('click', function () {

        yaCounter41594534.reachGoal('num3');

    });


    $('[data-url="tel:+375 17 322-95-55"]').on('click', function () {

        yaCounter41594534.reachGoal('num4');

    });


    $('[data-url="mailto:aerobikaby@gmail.com"]').on('click', function () {

        yaCounter41594534.reachGoal('mail');

    });


    $('[data-url="https://www.google.com.ua/maps/place/%D0%B2%D1%83%D0%BBi%D1%86%D0%B0+%D0%93%D1%80%D0%BE%D0%BC%D0%B0%D0%B2%D0%B0+14,+%D0%9C%D1%96%D0%BD%D1%81%D0%BA,+%D0%91%D1%96%D0%BB%D0%BE%D1%80%D1%83%D1%81%D1%8C/@53.8557382,27.4451083,17z/data=!3m1!4b1!4m5!3m4!1s0x46dbda7b460c1b77:0xb850f6e43988e00!8m2!3d53.8557382!4d27.447297"]').on('click', function () {

        yaCounter41594534.reachGoal('zal1');

    });


    $('[data-url="https://www.google.com.ua/maps/place/%D0%B2%D1%83%D0%BB%D1%96%D1%86%D0%B0+%D0%9A%D0%B0%D1%81%D0%BC%D0%B0%D0%BD%D0%B0%D1%9E%D1%82%D0%B0%D1%9E+56,+%D0%9C%D1%96%D0%BD%D1%81%D0%BA,+%D0%91%D1%96%D0%BB%D0%BE%D1%80%D1%83%D1%81%D1%8C/@53.8458731,27.4523208,17z/data=!3m1!4b1!4m5!3m4!1s0x46dbda78202f167f:0x41f9b88dae8549bd!8m2!3d53.84587!4d27.4545095"]').on('click', function () {

        yaCounter41594534.reachGoal('zal2');

    });


    $('[data-url="https://goo.gl/maps/LRD49Htxdrn"]').on('click', function () {

        yaCounter41594534.reachGoal('zal3');

    });


    $('[data-url="https://maps.google.com/maps?ll=53.854405,27.436134&z=16&t=m&hl=ru&gl=UA&mapclient=embed&q=%D1%83%D0%BB.%20%D0%A1%D0%BB%D0%BE%D0%B1%D0%BE%D0%B4%D1%81%D0%BA%D0%B0%D1%8F%2041%20%D0%9C%D0%B8%D0%BD%D1%81%D0%BA%20%D0%91%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C"]').on('click', function () {

        yaCounter41594534.reachGoal('zal4');

    });


    $('[data-url="https://goo.gl/maps/VAvQ3dwscXJ2"]').on('click', function () {

        yaCounter41594534.reachGoal('zal5');

    });

    var offset = 8;
    var category_id;
    $('#more_articles').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "/blog/?offsets=" + offset,
            dataType: 'json',
            success: function (result) {
                offset += 8;
                var html = $('.news-list>div').html();
                html += result.html;
                $('.news-list>div').html(html);
                if (!result.more) {
                    $('#more_articles').hide();
                }
            }
        });
    });

});
$(document).ready(function () {
    setTimeout(function(){
      $.ajax({
          type: "GET",
          url: "/blog?important=1",
          dataType: 'json',
          success: function (result) {
            if(result.html) {
                $('.items-nws-popup').html(result.html);
                setTimeout(function(){
                  $('#news').addClass('active');
                }, 1000);
              }
          }
      });
    }, 4000);

    $.ajax({
        type: "GET",
        url: "/blog?formain=1",
        dataType: 'json',
        success: function (result) {
            $('.news-index #this-news').html(result.html);
        }
    });
});
